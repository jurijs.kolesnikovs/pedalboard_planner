class PedalBoardModel {
  constructor({ pedalboard_id, title, width, height, type }) {
    this.id = Math.random().toString(36).substring(2, 15).toUpperCase() + Math.random().toString(36).substring(2, 15).toUpperCase()
    this.pedalboard_id = pedalboard_id
    this.title = title
    this.width = width
    this.height = height
    this.type = type
  }
}

export default PedalBoardModel